## Crypto Order Book Aggregator

### Overview
Provides an API service for pulling Order Books from multiple cryptocurrency exchanges, along with a front-end UI for viewing volume at various price points.

### Supported exchanges
The service currently supports Bitfinex, Bittrex, GDAX, and Poloniex. Not all of these exchanges support the same markets, so exchanges will only be queried if the currencies are two known, supported currencies for that exchange.

### Installation
To deploy the service:
1. Configure an AWS account with proper permissions to [allow Serverless access](https://serverless.com/framework/docs/providers/aws/guide).
2. Create a config.js file in the service folder with the following credentials (these can be obtained from setting up API access through the Bittrex console):

    ```javascript
    module.exports = {
      bittrexApiKey: '{apiKey}',
      bittrexApiSecret: '{secret}'
    }
    ```

3. Deploy the service using `sudo sls deploy`. This will create a new lambda function called cob-service-dev-api.
4. Set up API Gateway to call this function at an /api/{action} endpoint. Set up api access to serve static files from an S3 folder.

To deploy the site, just upload the index.html, index.css, and dist/main.js files to the above S3 folder.

### Testing
Tests are provided for all back-end modules, and can be executed via `npm test` in the service folder.

### Open Source Libraries
This code uses several open source libraries:
- [Chart.js](https://www.chartjs.org/)
- [Vue](https://vuejs.org/)
- [Webpack](https://webpack.js.org/)
- [Also, an awesome spinner SVG by Aurer from CodePen](https://codepen.io/aurer/pen/jEGbA)

### License
This library is available under the (MIT License)[https://opensource.org/licenses/MIT]