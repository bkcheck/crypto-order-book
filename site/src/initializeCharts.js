import Chart from 'chart.js'

/**
 * Creates the asks and bids Chart.js objects
 * @param {object} params.asksCanvas The canvas element for the asks chart.
 * @param {object} params.bidsCanvas The canvas element for the bids chart.
 */
function initializeCharts ({ asksCanvas, bidsCanvas }) {
  const options = {
    categoryPercentage: 1.0,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        scaleLabel: {
          display: true,
          fontColor: '#999',
          labelString: 'Rate'
        },
        stacked: true
      }],
      yAxes: [{
        scaleLabel: {
          display: true,
          fontColor: '#999',
          labelString: 'Volume'
        },
        stacked: true
      }]
    },
    tooltips: {
      callbacks: {
        label: function (tooltipItem, data) {
          var categorySum = data.datasets.map(dataset => dataset.data[tooltipItem.index]).reduce((acc, current) => acc + current)
          var roundedSum = Math.round(categorySum * 100) / 100
          var label = data.datasets[tooltipItem.datasetIndex].label || ''
          if (label) {
            label += ': '
          }
          label += Math.round(tooltipItem.yLabel * 100) / 100
          label += ' (Total: ' + String(roundedSum) + ')'
          return label
        }
      }
    }
  }

  const asksChart = new Chart(asksCanvas, {
    type: 'bar',
    options: Object.assign(options, { title: { display: true, fontColor: '#999', position: 'top', text: 'Asks' } })
  })

  const bidsChart = new Chart(bidsCanvas, {
    type: 'bar',
    options: Object.assign(options, { title: { display: true, position: 'top', text: 'Bids' } })
  })

  return { asksChart, bidsChart }
}

export default initializeCharts
