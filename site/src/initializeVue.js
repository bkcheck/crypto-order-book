import Vue from 'vue'
import { currencies } from './refData'

/**
 * Returns the Vue component that controls page interactions.
 * @param {object} params.asksChart The initialized Chart.js for asks
 * @param {object} params.bidsChart The initialized Chart.js for bids
 * @param {function} deps.getOrderBooks
 * @param {function} deps.getDataStats
 * @param {function} deps.formatData
 */
function initializeVue ({ asksChart, bidsChart }, deps) {
  const initialBaseCurrency = 'BTC'
  const initialMarketCurrency = 'ETH'
  const initialTierSize = 0.0001
  const initialTierSizes = [0.0001, 0.0005, 0.0001, 0.0005, 0.001]

  return new Vue({
    el: '#app',
    data: {
      baseCurrency: initialBaseCurrency,
      busy: true,
      currencies,
      marketCurrency: initialMarketCurrency,
      orderBooks: [],
      tierSize: initialTierSize,
      stats: { tierSizes: initialTierSizes }
    },
    computed: {
      allAsks: function () {
        let allOrders = []
        this.orderBooks.forEach(book => {
          allOrders = allOrders.concat(book.asks.map(order => Object.assign(order, { exchange: book.name })))
        })
        return allOrders.sort((a, b) => a.rate - b.rate)
      },
      allBids: function () {
        let allOrders = []
        this.orderBooks.forEach(book => {
          allOrders = allOrders.concat(book.bids.map(order => Object.assign(order, { exchange: book.name })))
        })
        return allOrders.sort((a, b) => b.rate - a.rate)
      },
      baseCurrencies: function () {
        return Object.keys(this.currencies)
      },
      marketCurrencies: function () {
        return this.currencies[this.baseCurrency]
      },
      tierSizes: function () {
        if (this.stats) {
          return this.stats.tierSizes
        }
        return [0.0001, 0.0005, 0.001, 0.005, 0.01]
      }
    },
    created: function () {
      this.getOrderBooks(this.baseCurrency, this.marketCurrency)
    },
    methods: {
      baseCurrencyChanged: function () {
        if (!this.marketCurrencies.includes(this.marketCurrency)) {
          this.marketCurrency = this.marketCurrencies[0]
        }
        this.getOrderBooks(this.baseCurrency, this.marketCurrency)
      },
      getOrderBooks: function (baseCurrency, marketCurrency) {
        this.busy = true

        deps.getOrderBooks({ baseCurrency, marketCurrency })
          .then(books => { this.orderBooks = books })
          .then(() => deps.getDataStats({ orderBooks: this.orderBooks }))
          .then(stats => {
            this.stats = stats
            this.tierSize = stats.tierSizes[2]
          })
          .then(() => deps.formatData({ orderBooks: this.orderBooks, stats: this.stats, tierSize: this.tierSize }))
          .then(updateCharts)
          .catch(err => window.alert(err))
          .then(() => { this.busy = false })
      },
      marketCurrencyChanged: function () {
        this.getOrderBooks(this.baseCurrency, this.marketCurrency)
      },
      tierSizeChanged: function () {
        this.busy = true

        let tierSize = parseFloat(this.tierSize)
        this.tierSize = tierSize
        let newData = deps.formatData({ orderBooks: this.orderBooks, stats: this.stats, tierSize: this.tierSize })
        updateCharts(newData)

        this.busy = false
      }
    }
  })

  function updateCharts ({ asks, bids }) {
    updateChart(asksChart, asks)
    updateChart(bidsChart, bids)
  }

  function updateChart (chart, data) {
    const { labels, datasets } = data

    chart.data.labels = labels
    chart.data.datasets = datasets
    chart.update()
  }
}

export default initializeVue
