const barColors = ['#D81B60', '#3949AB', '#00ACC1', '#43A047', '#FDD835', '#FB8C00']

/**
 * Formats the order book data for display in Chart.js
 * @param {object} params.orderBooks The returned order books from the API endpoint
 * @param {object} params.stats The data stats as computed by getDataStats
 * @param {number} params.tierSize The tier size to fit order into
 */
function formatData ({ orderBooks, stats, tierSize }) {
  return {
    asks: getDatasets('asks'),
    bids: getDatasets('bids')
  }

  function getDatasetForOrderBook (orderBook, rateTiers, type, bookIndex) {
    let name = orderBook.name
    return {
      label: name[0].toUpperCase() + name.slice(1),
      backgroundColor: barColors[bookIndex],
      data: fitOrdersToTiers(orderBook, rateTiers, type, bookIndex)
    }
  }

  function fitOrderToTiers (order, rateTiers, rates) {
    for (var i = 0; i < rateTiers.length; i++) {
      let nextRateTier = rateTiers[i + 1]
      if (nextRateTier) {
        if (order.rate < nextRateTier) {
          rates[i] += order.quantity
          return
        }
      } else {
        rates[i] += order.quantity
        return
      }
    }
  }

  function fitOrdersToTiers (book, rateTiers, type) {
    let orders = book[type]
    let rates = rateTiers.map(x => 0)

    orders.forEach(order => fitOrderToTiers(order, rateTiers, rates))

    return rates
  }

  function getDatasets (type) {
    const minRate = stats[type].min
    const maxRate = stats[type].max
    const rateTiers = getRateTiers(minRate, maxRate, tierSize) // Select middle tier size to start

    if (!rateTiers.length) {
      // Unlikely to be hit. Would only occur if no order books had any orders for the given type.
      return []
    }

    // Filtering here will prevent labels for orderless exchanges appearing on the charts
    // in the case of an exchange not supporting the currency combo.
    let booksWithOrders = orderBooks.filter(book => book.asks.length || book.bids.length)
    let datasets = booksWithOrders.map((orderBook, index) => getDatasetForOrderBook(orderBook, rateTiers, type, index))
    return {
      labels: rateTiers.map(t => String(t)),
      datasets
    }
  }

  function getRateTiers (minRate, maxRate, tierSize) {
    const precisionAdjustment = 1e8

    // Decimal precision on all rates returned by Bittrex or Poloniex
    // seems to never exceed 8 decimal places. Multiply all rates by
    // 1e8 and round so we don't run into any weird floating point issues.
    const minAdjustedRate = adjustParam(minRate)
    const maxAdjustedRate = adjustParam(maxRate)
    const adjustedTierSize = adjustParam(tierSize)

    // Calculate the lowest rate tier and fill our tier array starting from there.
    let currentTier = getLowestRateTier()
    let rateTiers = []
    while (currentTier < maxAdjustedRate) {
      rateTiers.push(currentTier / precisionAdjustment)
      currentTier += adjustedTierSize
    }

    return rateTiers

    function adjustParam (val) {
      return Math.round(val * precisionAdjustment)
    }

    function getLowestRateTier () {
      return minAdjustedRate - (minAdjustedRate % adjustedTierSize)
    }
  }
}

export default formatData
