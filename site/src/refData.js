const currencies = {
  'BTC': ['LTC', 'DOGE', 'ETH', 'ZEC'],
  'USDT': ['BTC', 'ETH', 'ZEC', 'LTC'],
  'ETH': ['ZEC']
}

module.exports.currencies = currencies
