/**
 * Returns the min, max, and suggested x-axis price tiers for the supplied order books.
 * @param {object} params.orderBooks
 */
function getDataStats ({ orderBooks }) {
  const stats = {
    asks: {
      min: getMinRate('asks'),
      max: getMaxRate('asks')
    },
    bids: {
      min: getMinRate('bids'),
      max: getMaxRate('bids')
    }
  }

  // Asks and bids should be on the same order of magnitude here so let's just
  // use the asks stats to compute tier sizes
  return Object.assign(stats, { tierSizes: getTierSizes(stats.asks.min, stats.asks.max) })

  function getMaxRate (type) {
    let maxRate
    orderBooks.forEach(book => {
      // The ask and bid arrays are already sorted for each book
      let orders = book[type]

      if (orders.length) {
        let highestRateOrder = orders[orders.length - 1]
        if (!maxRate || highestRateOrder.rate > maxRate) {
          maxRate = highestRateOrder.rate
        }
      }
    })

    if (!maxRate) {
      throw new Error(`Could not determine maximum ${type} rate.`)
    }

    return maxRate
  }

  function getMinRate (type) {
    let minRate
    orderBooks.forEach(book => {
      let orders = book[type]

      if (orders.length) {
        let lowestRateOrder = orders[0]

        if (!minRate || lowestRateOrder.rate < minRate) {
          minRate = lowestRateOrder.rate
        }
      }
    })

    if (!minRate) {
      throw new Error(`Could not determine minimum ${type} rate.`)
    }

    return minRate
  }

  function getTierSizes (min, max) {
    const tiers = 20
    // Estimate the default number of tiers to show by finding the tier
    // size that will display closest to {tiers} discrete chart subdivisions
    let subdivision = (max - min) / tiers

    // Need to find roughly the order of magnitude of the chart tiers
    let n = Math.round(Math.log10(subdivision))
    let t = Math.pow(10, n)

    // Allow 2 logarithmically smaller and larger values on either side
    return [t / 10, t / 2, t, t * 5, t * 10]
  }
}

export default getDataStats
