import formatData from './formatData'
import getDataStats from './getDataStats'
import getOrderBooks from './getOrderBooks'
import initializeCharts from './initializeCharts'
import initializeVue from './initializeVue'

/**
 * The entry point for the webpack bundle.
 */
function initializeApp () {
  const asksCanvas = document.getElementById('asksChart')
  const bidsCanvas = document.getElementById('bidsChart')
  Promise.resolve({ asksCanvas, bidsCanvas })
    .then(initializeCharts)
    .then(charts => initializeVue(charts, { formatData, getOrderBooks, getDataStats }))
}

export default initializeApp()
