const apiBase = 'https://crypto.billkowalczyk.com/api/'

/**
 * Calls an API endpoint to pull order book data from each available exchange.
 * @param {string} params.baseCurrency
 * @param {string} params.marketCurrency
 */
function getOrderBooks (params) {
  const { baseCurrency, marketCurrency } = params
  const uri = `${apiBase}getAllOrderBooks?baseCurrency=${baseCurrency}&marketCurrency=${marketCurrency}`
  return window.fetch(uri).then(response => response.json())
}

export default getOrderBooks
