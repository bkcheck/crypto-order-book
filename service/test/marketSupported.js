const test = require('tape')
const mod = require('../src/marketSupported')

test('Should pass on supported market', t => {
  let params = {
    markets: {
      'base': ['market']
    },
    baseCurrency: 'base',
    marketCurrency: 'market'
  }

  t.plan(1)

  let response = mod(params)
  t.strictEqual(response, true, 'Module response OK')
  t.end()
})

test('Should return false for unsupported market currency', t => {
  let params = {
    markets: {
      'base': ['market']
    },
    baseCurrency: 'base',
    marketCurrency: 'notsupported'
  }

  t.plan(1)

  let response = mod(params)
  t.strictEqual(response, false, 'Module response OK')
  t.end()
})

test('Should return false for unsupported base currency', t => {
  let params = {
    markets: {
      'base': ['market']
    },
    baseCurrency: 'notsupported',
    marketCurrency: 'market'
  }

  t.plan(1)

  let response = mod(params)
  t.strictEqual(response, false, 'Module response OK')
  t.end()
})
