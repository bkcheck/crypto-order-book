const test = require('tape')
const mod = require('../src/getAllOrderBooks')

function orderBookGenerator (name, t) {
  return (actual, deps) => {
    t.deepEqual(actual, { baseCurrency: 'base', marketCurrency: 'market' }, name + ' params')
    t.ok(deps, name + ' deps')
    return Promise.resolve({
      name,
      asks: [],
      bids: []
    })
  }
}

function defaultDeps () {
  return {
    getBitfinexOrderBook: {},
    getBittrexOrderBook: {},
    getGdaxOrderBook: {},
    getPoloniexOrderBook: {}
  }
}

test('Should get order books from all exchanges', t => {
  let params = { baseCurrency: 'base', marketCurrency: 'market' }
  let deps = defaultDeps()
  deps.getBitfinexOrderBook = orderBookGenerator('bitfinex', t)
  deps.getBittrexOrderBook = orderBookGenerator('bittrex', t)
  deps.getGdaxOrderBook = orderBookGenerator('GDAX', t)
  deps.getPoloniexOrderBook = orderBookGenerator('poloniex', t)

  t.plan(9)

  let expected = [
    { name: 'bitfinex', asks: [], bids: [] },
    { name: 'bittrex', asks: [], bids: [] },
    { name: 'GDAX', asks: [], bids: [] },
    { name: 'poloniex', asks: [], bids: [] }
  ]

  mod(params, deps)
    .then(result => t.deepEqual(result, expected, 'Module response'))
    .catch(t.fail)
    .then(t.end)
})
