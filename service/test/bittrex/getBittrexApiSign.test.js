const test = require('tape')
const mod = require('../../src/bittrex/getBittrexApiSign')

function defaultDeps () {
  return {
    crypto: {
      createHmac: {}
    },
    bittrexApiSecret: 'bittrexSecret'
  }
}

test('Should call crypto module with correct secret', t => {
  const params = { uri: 'uri' }
  let deps = defaultDeps()
  deps.crypto.createHmac = (algorithm, secret) => {
    t.equal(secret, 'bittrexSecret', 'bittrexSecret')
    return {
      update: uri => {
        t.equal(uri, 'uri', 'Correct URI')
      },
      digest: () => {
        return 'encrypted'
      }
    }
  }

  t.plan(3)

  let result = mod(params, deps)
  t.equal(result, 'encrypted', 'Return value')
  t.end()
})
