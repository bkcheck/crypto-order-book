const test = require('tape')
const mod = require('../../src/bittrex/getBittrexOrderBook')

function defaultDeps () {
  return {
    callBittrex: {},
    marketSupported: {}
  }
}

test('Should call Bittrex with correct params', t => {
  let params = { baseCurrency: 'base', marketCurrency: 'market' }
  let deps = defaultDeps()
  deps.marketSupported = actual => {
    t.equal(actual.baseCurrency, 'base', 'marketSupported baseCurrency')
    t.equal(actual.marketCurrency, 'market', 'marketSupported marketCurrency')
    return true
  }
  deps.callBittrex = (actual, deps) => {
    let expected = {
      method: 'getorderbook',
      params: {
        market: 'base-market',
        type: 'both'
      }
    }
    t.deepEqual(actual, expected, 'callBittrex params')
    t.ok(deps, 'callBittrex deps')

    return Promise.resolve({ sell: [], buy: [] })
  }

  t.plan(5)

  mod(params, deps)
    .then(result => t.deepEqual(result, { name: 'bittrex', asks: [], bids: [] }))
    .catch(t.fail)
    .then(t.end)
})

test('Should return empty order arrays on unsupported market', t => {
  let params = { baseCurrency: 'base', marketCurrency: 'market' }
  let deps = defaultDeps()
  deps.marketSupported = () => false

  t.plan(1)

  mod(params, deps)
    .then(result => t.deepEqual(result, { name: 'bittrex', asks: [], bids: [] }))
    .catch(t.fail)
    .then(t.end)
})
