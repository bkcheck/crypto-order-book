const test = require('tape')
const mod = require('../../src/bittrex/callBittrex')

function defaultDeps () {
  return {
    getBittrexUri: {},
    getBittrexApiSign: {},
    https: {
      request: {}
    }
  }
}

test('Should call Bittrex with correct uri', t => {
  let params = {
    method: 'method',
    params: 'params'
  }

  let deps = defaultDeps()
  deps.getBittrexUri = (actual, deps) => {
    let expected = {
      method: 'method',
      params: 'params'
    }
    t.deepEqual(actual, expected, 'getBittrexUri params')
    t.ok(deps, 'getBittrexUri deps')

    return 'bittrexUri'
  }
  deps.getBittrexApiSign = (actual, deps) => {
    let expected = { uri: 'bittrexUri' }
    t.deepEqual(actual, expected, 'getBittrexApiSign params')
    t.ok(deps, 'getBittrexApiSign deps')

    return 'bittrexApiSign'
  }
  deps.https.request = (uri, cb) => {
    t.equal(uri, 'bittrexUri')

    cb({
      setEncoding: () => {},
      statusCode: 200,
      on: (event, callback) => {
        if (event === 'end') {
          callback()
        } else {
          callback('{"success":true}')
        }
      }
    })

    return {
      setHeader: (name, sign) => {
        t.equal(name, 'apisign', 'setHeader name')
        t.equal(sign, 'bittrexApiSign', 'setHeader apisign')
      },
      on: () => {},
      end: () => {}
    }
  }

  t.plan(8)

  mod(params, deps)
    .then(t.pass, t.fail)
    .then(t.end)
})

test('Should fail if bittrex returns success = false', t => {
  let params = {
    method: 'method',
    params: 'params'
  }

  let deps = defaultDeps()
  deps.getBittrexUri = (actual, deps) => {
    return 'bittrexUri'
  }
  deps.getBittrexApiSign = (actual, deps) => {
    return 'bittrexApiSign'
  }
  deps.https.request = (uri, cb) => {
    cb({
      setEncoding: () => {},
      statusCode: 200,
      on: (event, callback) => {
        if (event === 'end') {
          callback()
        } else {
          callback('{"success":false,"message":"message"}')
        }
      }
    })

    return {
      setHeader: () => {},
      on: () => {},
      end: () => {}
    }
  }

  t.plan(1)

  mod(params, deps)
    .then(t.fail, err => t.equal(err, 'message', 'Response message'))
    .then(t.end)
})
