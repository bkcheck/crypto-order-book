const test = require('tape')
const mod = require('../../src/gdax/getGdaxOrderBook')

function defaultDeps () {
  return {
    callApi: {},
    marketSupported: {}
  }
}

test('Should call GDAX and format response correctly', t => {
  let params = { baseCurrency: 'USDT', marketCurrency: 'BTC' }
  let deps = defaultDeps()
  deps.marketSupported = actual => {
    t.equal(actual.baseCurrency, 'USDT', 'marketSupported baseCurrency')
    t.equal(actual.marketCurrency, 'BTC', 'marketSupported marketCurrency')
    return true
  }
  deps.callApi = (actual, deps) => {
    let expected = {
      hostname: 'api.gdax.com',
      path: '/products/BTC-USD/book?level=2'
    }
    t.deepEqual(actual, expected, 'callApi params')
    t.ok(deps, 'callApi deps')
    return Promise.resolve({
      asks: [['100', '200']],
      bids: []
    })
  }

  t.plan(5)

  let expected = {
    name: 'GDAX',
    asks: [{ rate: 100, quantity: 200 }],
    bids: []
  }

  mod(params, deps)
    .then(result => t.deepEqual(result, expected, 'Module response'))
    .catch(t.fail)
    .then(t.end)
})

test('Should not call API if market not supported', t => {
  let params = { baseCurrency: 'base', marketCurrency: 'market' }
  let deps = defaultDeps()
  deps.marketSupported = () => false
  deps.callApi = () => {
    t.fail('Should not have called API')
  }

  t.plan(1)

  mod(params, deps)
    .then(() => t.pass('Module success'), t.fail)
    .then(t.end)
})
