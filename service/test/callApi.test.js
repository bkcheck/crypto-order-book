const test = require('tape')
const mod = require('../src/callApi')

function defaultDeps () {
  return {
    https: {
      request: {}
    }
  }
}

test('Should call the URI and pass on 200 response', t => {
  let params = { hostname: 'hostname', path: 'path' }
  let deps = defaultDeps()
  deps.https.get = (actual, cb) => {
    let expected = {
      hostname: 'hostname',
      path: 'path',
      headers: { 'User-Agent': 'nodejs' }
    }
    t.deepEqual(actual, expected, 'https params')

    cb({
      setEncoding: () => {},
      statusCode: 200,
      on: (event, callback) => {
        if (event === 'end') {
          callback()
        } else {
          callback('{}')
        }
      }
    })
  }

  t.plan(2)

  mod(params, deps)
    .then(t.pass, t.fail)
    .then(t.end)
})
