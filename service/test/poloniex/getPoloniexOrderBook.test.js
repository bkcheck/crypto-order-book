const test = require('tape')
const mod = require('../../src/poloniex/getPoloniexOrderBook')

function defaultDeps () {
  return {
    callApi: {},
    marketSupported: {}
  }
}

test('Should call Poloniex and format response correctly', t => {
  let params = { baseCurrency: 'USDT', marketCurrency: 'BTC' }
  let deps = defaultDeps()
  deps.marketSupported = actual => {
    t.equal(actual.baseCurrency, 'USDT', 'marketSupported baseCurrency')
    t.equal(actual.marketCurrency, 'BTC', 'marketSupported marketCurrency')
    return true
  }
  deps.callApi = (actual, deps) => {
    let expected = {
      hostname: 'poloniex.com',
      path: '/public?command=returnOrderBook&currencyPair=USDT_BTC&depth=100'
    }
    t.deepEqual(actual, expected, 'callApi params')
    t.ok(deps, 'callApi deps')
    return Promise.resolve({
      asks: [[ '100', 200 ]],
      bids: []
    })
  }

  t.plan(5)

  let expected = {
    name: 'poloniex',
    asks: [{ rate: 100, quantity: 200 }],
    bids: []
  }

  mod(params, deps)
    .then(result => t.deepEqual(result, expected, 'Module result'))
    .catch(t.fail)
    .then(t.end)
})

test('Should not call API if market not supported', t => {
  let params = { baseCurrency: 'base', marketCurrency: 'market' }
  let deps = defaultDeps()
  deps.marketSupported = () => false
  deps.callApi = () => {
    t.fail('Should not have called API')
  }

  t.plan(1)

  mod(params, deps)
    .then(() => t.pass('Module success'), t.fail)
    .then(t.end)
})
