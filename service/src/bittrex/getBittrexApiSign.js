const assert = require('assert')

/**
 * Returns the apisign header for calling the Bittrex API
 * @param {string} params.uri The full bittrex URI to sign
 * @param {object} deps.crypto
 * @param {function} deps.bittrexApiSecret
 */
function getBittrexApiSign (params, deps) {
  assert(params, 'params')
  assert(params.uri, 'params.uri')
  assert(deps, 'deps')
  assert(deps.crypto, 'deps.crypto')
  assert(deps.bittrexApiSecret, 'deps.bittrexApiSecret')

  const hmac = deps.crypto.createHmac('sha512', deps.bittrexApiSecret)

  hmac.update(params.uri)
  return hmac.digest('hex')
}

module.exports = getBittrexApiSign
