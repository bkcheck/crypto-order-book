const assert = require('assert')

const markets = {
  'BTC': ['LTC', 'DOGE', 'ETH', 'ZEC'],
  'USDT': ['BTC', 'ETH', 'ZEC', 'LTC'],
  'ETH': ['ZEC']
}

/**
 * Returns a full order book from Bittrex.
 * @param {string} params.baseCurrency
 * @param {string} params.marketCurrency
 * @param {function} deps.callBittrex
 * @param {function} deps.marketSupported
 */
async function getBittrexOrderBook (params, deps) {
  assert(params, 'params')
  assert(params.baseCurrency, 'params.baseCurrency')
  assert(params.marketCurrency, 'params.marketCurrency')
  assert(deps, 'deps')
  assert(deps.callBittrex, 'deps.callBittrex')
  assert(deps.marketSupported, 'deps.marketSupported')

  let data = { name: 'bittrex', asks: [], bids: [] }

  if (!deps.marketSupported({ markets, baseCurrency: params.baseCurrency, marketCurrency: params.marketCurrency })) {
    return data
  }

  const market = `${params.baseCurrency}-${params.marketCurrency}`
  const opts = {
    method: 'getorderbook',
    params: { market, type: 'both' }
  }

  const response = await deps.callBittrex(opts, deps)
  return Object.assign(data, formatResponse())

  function formatResponse () {
    return {
      asks: formatArray(response.sell),
      bids: formatArray(response.buy)
    }
  }

  function formatArray (arr) {
    return arr
      .map(o => {
        return { quantity: o.Quantity, rate: o.Rate }
      })
      .sort((a, b) => a.rate - b.rate)
  }
}

module.exports = getBittrexOrderBook
