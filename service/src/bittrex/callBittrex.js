const assert = require('assert')

/**
 * Sends an HTTP request to the Bittrex API
 * @param {string} params.method The API method to call
 * @param {object} params.params Query string params for the request
 * @param {object} deps.https
 * @param {function} deps.getBittrexUri
 * @param {function} deps.getBittrexApiSign
 */
function callBittrex (params, deps) {
  assert(params, 'params')
  assert(params.method, 'params.method')
  assert(params.params, 'params.params')
  assert(deps, 'deps')
  assert(deps.https, 'deps.https')
  assert(deps.getBittrexUri, 'deps.getBittrexUri')
  assert(deps.getBittrexApiSign, 'deps.getBittrexApiSign')

  const uri = deps.getBittrexUri(params, deps)
  const apiSign = deps.getBittrexApiSign({ uri }, deps)

  return call()

  function call () {
    return new Promise((resolve, reject) => {
      const req = deps.https.request(uri, res => {
        if (res.statusCode === 200) {
          let data = ''

          res.setEncoding('utf8')
          res.on('data', chunk => { data += chunk })
          res.on('end', () => {
            let json = JSON.parse(data)

            if (json.success) {
              resolve(json.result)
            } else {
              reject(json.message)
            }
          })
        } else {
          reject(new Error(`Bittrex responded with error code: ${res.statusCode}`))
        }
      })

      req.setHeader('apisign', apiSign)
      req.on('error', e => reject(e))

      req.end()
    })
  }
}

module.exports = callBittrex
