const assert = require('assert')
const { encode } = require('querystring')

const baseUri = 'https://bittrex.com/api/v1.1/public'

/**
 * Gets the full URI for calling the Bittrex API
 * @param {string} params.method The API method to call
 * @param {object} params.params Query string params for the method call
 * @param {string} deps.bittrexApiKey
 */
function getBittrexUri (params, deps) {
  assert(params, 'params')
  assert(params.method, 'params.method')
  assert(params.params, 'params.params')
  assert(deps, 'deps')
  assert(deps.bittrexApiKey, 'deps.bittrexApiKey')

  const authParams = {
    apiKey: deps.bittrexApiKey,
    nonce: Math.floor(new Date() / 1000)
  }
  const queryStringParams = Object.assign(params.params, authParams)
  const queryString = encode(queryStringParams)
  return `${baseUri}/${params.method}?${queryString}`
}

module.exports = getBittrexUri
