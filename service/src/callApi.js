const assert = require('assert')

/**
 * A helper module for calling a GET API endpoint
 * @param {string} params.hostname
 * @param {string} params.path
 * @param {object} deps.https
 */
function callApi (params, deps) {
  assert(params, 'params')
  assert(params.hostname, 'params.hostname')
  assert(params.path, 'params.path')
  assert(deps, 'deps')
  assert(deps.https, 'deps.https')

  return new Promise((resolve, reject) => {
    let opts = {
      hostname: params.hostname,
      path: params.path,
      headers: {
        'User-Agent': 'nodejs'
      }
    }
    deps.https.get(opts, res => {
      let data = ''

      res.setEncoding('utf8')
      res.on('data', chunk => { data += chunk })

      if (res.statusCode === 200) {
        res.on('end', () => {
          resolve(JSON.parse(data))
        })
      } else {
        reject(new Error(`${params.uri} responded with error code: ${res.statusCode}. ${data}`))
      }
    })
  })
}

module.exports = callApi
