const routes = {
  getAllOrderBooks_GET: {
    action: 'getAllOrderBooks',
    getData: (body, params) => {
      return {
        baseCurrency: params.baseCurrency,
        marketCurrency: params.marketCurrency
      }
    }
  }
}

module.exports = routes
