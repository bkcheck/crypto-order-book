const assert = require('assert')
const { format } = require('util')
const routes = require('./routes')
const ApiError = require('./ApiError')

function getHeaders () {
  return {
    'Content-Type': 'application/json'
  }
}

/**
 * Handles incoming requests from API Gateway > Lambda
 * @param {*} event The Lambda event object (from API Gateway)
 * @param {*} context The Lambda context object
 * @param {*} callback The Lambda callback (for completing function execution)
 */
async function routeRequest (event, context, callback, deps) {
  assert(event, 'event')
  assert(context, 'context')
  assert(callback, 'callback')

  const params = event.queryStringParameters || {}
  const action = event.pathParameters.action
  params.id = event.pathParameters.id || null

  try {
    const route = getRoute()
    const result = await executeRoute(route)
    respondSuccess(result)
  } catch (e) {
    respondFail(e)
  }

  function executeRoute (route) {
    if (route.action === 'noop') {
      return
    }

    const fn = deps[route.action]

    if (!fn) {
      throw new ApiError()
    }

    let fnParams = {}
    if (route.getData) {
      fnParams = route.getData(JSON.parse(event.body), params)
    }

    return fn(fnParams, deps)
  }

  function getRoute () {
    if (action === null) {
      throw new ApiError(400, 'No action provided.')
    }

    const method = event.requestContext.httpMethod
    const routeName = format('%s_%s', (method === 'OPTIONS' ? '*' : action), method)
    const route = routes[routeName]

    if (!route) {
      throw new ApiError(400, 'Invalid method/action: ' + routeName)
    }

    return route
  }

  function respondFail (err) {
    const message = err.message || 'An error occurred while processing this request.'

    console.error(err)

    const response = {
      isBase64Encoded: false,
      statusCode: err.responseCode || 500,
      headers: getHeaders(),
      body: JSON.stringify({ message })
    }

    callback(null, response)
  }

  function respondSuccess (data) {
    const response = {
      isBase64Encoded: false,
      statusCode: 200,
      headers: getHeaders(),
      body: JSON.stringify(data)
    }

    callback(null, response)
  }
}

module.exports = routeRequest
