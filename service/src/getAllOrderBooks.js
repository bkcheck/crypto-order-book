const assert = require('assert')

/**
 * Returns full order books from all supported exchanges.
 * @param {string} params.baseCurrency
 * @param {string} params.marketCurrency
 * @param {function} deps.getBitfinexOrderBook
 * @param {function} deps.getBittrexOrderBook
 * @param {function} deps.getGdaxOrderBook
 * @param {function} deps.getPoloniexOrderBook
 */
async function getAllOrderBooks (params, deps) {
  assert(params, 'params')
  assert(params.baseCurrency, 'params.baseCurrency')
  assert(params.marketCurrency, 'params.marketCurrency')
  assert(deps, 'deps')
  assert(deps.getBitfinexOrderBook, 'deps.getBitfinexOrderBook')
  assert(deps.getBittrexOrderBook, 'deps.getBittrexOrderBook')
  assert(deps.getGdaxOrderBook, 'deps.getGdaxOrderBook')
  // assert(deps.getGeminiOrderBook, 'deps.getGeminiOrderBook')
  assert(deps.getPoloniexOrderBook, 'deps.getPoloniexOrderBook')

  // Opted to remove Gemini as an exchange for now. Their rates were
  // off compared to all other services.
  return Promise.all([
    deps.getBitfinexOrderBook(params, deps),
    deps.getBittrexOrderBook(params, deps),
    deps.getGdaxOrderBook(params, deps),
    // deps.getGeminiOrderBook(params, deps),
    deps.getPoloniexOrderBook(params, deps)
  ])
}

module.exports = getAllOrderBooks
