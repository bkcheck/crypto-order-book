const assert = require('assert')

const markets = {
  'BTC': ['LTC', 'DOGE', 'ETH', 'ZEC'],
  'USDT': ['BTC', 'ETH', 'ZEC', 'LTC'],
  'ETH': ['ZEC']
}

/**
 * Returns a full order book from Poloniex.
 * @param {string} params.baseCurrency
 * @param {string} params.marketCurrency
 * @param {function} deps.callBittrex
 * @param {function} deps.marketSupported
 */
async function getPoloniexOrderBook (params, deps) {
  assert(params, 'params')
  assert(params.baseCurrency, 'params.baseCurrency')
  assert(params.marketCurrency, 'params.marketCurrency')
  assert(deps, 'deps')
  assert(deps.callApi, 'deps.callApi')
  assert(deps.marketSupported, 'deps.marketSupported')

  const { baseCurrency, marketCurrency } = params

  const data = { name: 'poloniex', asks: [], bids: [] }

  if (!deps.marketSupported({ markets, baseCurrency: params.baseCurrency, marketCurrency: params.marketCurrency })) {
    return data
  }

  const hostname = 'poloniex.com'
  const path = `/public?command=returnOrderBook&currencyPair=${baseCurrency}_${marketCurrency}&depth=100`

  const response = await deps.callApi({ hostname, path }, deps)
  return Object.assign(data, formatResponse())

  function formatResponse () {
    return {
      asks: mapArraysToObjects(response.asks),
      bids: mapArraysToObjects(response.bids)
    }
  }

  function mapArraysToObjects (arrays) {
    return arrays
      .map(([rate, quantity]) => {
        return { rate: parseFloat(rate), quantity }
      })
      .sort((a, b) => a.rate - b.rate)
  }
}

module.exports = getPoloniexOrderBook
