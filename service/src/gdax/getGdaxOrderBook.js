const assert = require('assert')

const markets = {
  'BTC': ['ETH', 'LTC'],
  'USDT': ['BTC', 'ETH', 'LTC']
}

/**
 * Returns a full order book from GDAX.
 * @param {string} params.baseCurrency
 * @param {string} params.marketCurrency
 * @param {function} deps.callBittrex
 * @param {function} deps.marketSupported
 */
async function getGdaxOrderBook (params, deps) {
  assert(params, 'params')
  assert(params.baseCurrency, 'params.baseCurrency')
  assert(params.marketCurrency, 'params.marketCurrency')
  assert(deps, 'deps')
  assert(deps.callApi, 'deps.callApi')
  assert(deps.marketSupported, 'deps.marketSupported')

  let data = { name: 'GDAX', asks: [], bids: [] }

  if (!deps.marketSupported({ markets, baseCurrency: params.baseCurrency, marketCurrency: params.marketCurrency })) {
    return data
  }

  const baseCurrency = transformCurrencyForGdax(params.baseCurrency)
  const marketCurrency = transformCurrencyForGdax(params.marketCurrency)

  const hostname = 'api.gdax.com'
  const path = `/products/${marketCurrency}-${baseCurrency}/book?level=2`

  const response = await deps.callApi({ hostname, path }, deps)
  return Object.assign(data, formatResponse())

  function formatOrderArray (orders) {
    return orders && orders.map(order => {
      const [rate, quantity] = order
      return {
        rate: parseFloat(rate),
        quantity: parseFloat(quantity)
      }
    })
  }

  function formatResponse () {
    return {
      asks: formatOrderArray(response.asks) || [],
      bids: formatOrderArray(response.bids) || []
    }
  }

  function transformCurrencyForGdax (currency) {
    if (currency === 'USDT') {
      return 'USD'
    }
    return currency
  }
}

module.exports = getGdaxOrderBook
