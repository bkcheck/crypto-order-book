const assert = require('assert')

/**
 * Determines if a given market is supported based on the exchange's market list
 * @param {string} params.baseCurrency
 * @param {object} params.markets
 * @param {string} params.marketCurrency
 */
function marketSupported (params) {
  assert(params, 'params')
  assert(params.baseCurrency, 'params.baseMarket')
  assert(params.markets, 'params.markets')
  assert(params.marketCurrency, 'params.marketCurrency')

  const baseMarkets = params.markets[params.baseCurrency]
  if (baseMarkets && baseMarkets.find(x => x === params.marketCurrency)) {
    return true
  }
  return false
}

module.exports = marketSupported
