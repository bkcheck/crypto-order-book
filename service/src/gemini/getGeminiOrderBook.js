const assert = require('assert')

const markets = {
  'BTC': ['ETH', 'ZEC'],
  'USDT': ['BTC', 'ETH', 'ZEC'],
  'ETC': ['ZEC']
}

/**
 * Returns a full order book from Gemini.
 * @param {string} params.baseCurrency
 * @param {string} params.marketCurrency
 * @param {function} deps.callBittrex
 * @param {function} deps.marketSupported
 */
async function getGeminiOrderBook (params, deps) {
  assert(params, 'params')
  assert(params.baseCurrency, 'params.baseCurrency')
  assert(params.marketCurrency, 'params.marketCurrency')
  assert(deps, 'deps')
  assert(deps.callApi, 'deps.callApi')
  assert(deps.marketSupported, 'deps.marketSupported')

  const data = { name: 'gemini', asks: [], bids: [] }

  if (!deps.marketSupported({ markets, baseCurrency: params.baseCurrency, marketCurrency: params.marketCurrency })) {
    return data
  }

  const baseCurrency = transformCurrencyForGemini(params.baseCurrency)
  const marketCurrency = transformCurrencyForGemini(params.marketCurrency)

  const hostname = 'api.gemini.com'
  const path = `/v1/book/${marketCurrency}${baseCurrency}`

  const response = await deps.callApi({ hostname, path }, deps)
  return Object.assign(data, formatResponse())

  function formatOrderArray (orders) {
    return orders && orders.map(order => {
      return {
        rate: parseFloat(order.price),
        quantity: parseFloat(order.amount)
      }
    })
  }

  function formatResponse () {
    return {
      asks: formatOrderArray(response.asks) || [],
      bids: formatOrderArray(response.bids) || []
    }
  }

  function transformCurrencyForGemini (currency) {
    if (currency === 'USDT') {
      return 'usd'
    }
    return currency.toLowerCase()
  }
}

module.exports = getGeminiOrderBook
