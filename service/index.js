const routeRequest = require('./src/routeRequest')
const config = require('./config')
const modules = require('./modules')

exports.api = (event, context, callback) => {
  const deps = Object.assign(modules, config)
  routeRequest(event, context, callback, deps)
}
