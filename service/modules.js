const modules = {
  crypto: require('crypto'),
  https: require('https'),
  callApi: require('./src/callApi'),
  callBittrex: require('./src/bittrex/callBittrex'),
  getAllOrderBooks: require('./src/getAllOrderBooks'),
  getBitfinexOrderBook: require('./src/bitfinex/getBitfinexOrderBook'),
  getBittrexApiSign: require('./src/bittrex/getBittrexApiSign'),
  getBittrexOrderBook: require('./src/bittrex/getBittrexOrderBook'),
  getBittrexUri: require('./src/bittrex/getBittrexUri'),
  getGdaxOrderBook: require('./src/gdax/getGdaxOrderBook'),
  getGeminiOrderBook: require('./src/gemini/getGeminiOrderBook'),
  getPoloniexOrderBook: require('./src/poloniex/getPoloniexOrderBook'),
  marketSupported: require('./src/marketSupported')
}

module.exports = modules
